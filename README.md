# Medqor Coding Tests

## Back End

Creating CSVs is handled by the CSV_Backend class, and the yaml file creation is handled by the YAML_Backend class. Both classes inherit from a base class Backend, which handles loading the spacex.json file and ensuring the output file exists. The formatting of the json to the csv/yaml formats is handled by their respective classes in the "format" method. The methods "format" and "save" must be overridden from the base class. 

### Usage
From the src directory, run the command below. Two files, spacex.csv and spacex.yaml, will be generated in the src/output directory.

```bash
php -f main.php
```

 &nbsp;
## Front End

The frontend project can be seen from the frontend.html file in the src directory. Bootstrap was used for basic styling, and jQuery for a javascript library. 

When the page loads, the ids and names are fetched for all the launches in the spacex.json file, and populates the "Launch Site" select box. Usually I would use an API for this, but here it is saved in memory. Then, the first launch's data is fetched and rendered on the page. When the select box is changed, the newly selected launch's data is fetched and rendered on the page.

 &nbsp;
