<?php
    require_once( __DIR__ . "/../vendor/autoload.php" );
    require_once( __DIR__ . "/../src/backend.php" );
    require_once( __DIR__ . "/../src/csv_backend.php" );
    require_once( __DIR__ . "/../src/yaml_backend.php" );
    require_once( __DIR__ . "/../src/custom_error.php" );

    define( "SPACEX_JSON", __DIR__ . "/../src/data/spacex.json" );
    define( "CSV_OUTPUT_FILE", __DIR__ . "/../src/output/spacex.csv" );
    define( "YAML_OUTPUT_FILE", __DIR__ . "/../src/output/spacex.yaml" );
?>