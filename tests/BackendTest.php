<?php

    declare(strict_types=1);
    use PHPUnit\Framework\TestCase;

    final class BackendTest extends TestCase {

        public function test_is_assoc(){
            $backend = new CSV_Backend( false );
            $this->assertTrue( false === $backend->is_assoc( array( "one", "two", "three" ) ) );
            $this->assertTrue( true === $backend->is_assoc( array( "home" => "casa", "dog" => "pero" ) ) );
        }

        public function test_parse_keys(){
            $arr = array(
                "home" => "casa",
                "group" => array(
                    "working" => "haciendo",
                    "again" => "otra vez",
                    "white" => array( "one", "two", "three" )
                ),
                "tired" => "cansado",
                "arr" => array( "uno", "dos", "tres" ),
                "cool" => "chevre"
            );
            $expected_result = array( "home", "working", "again", "white", "tired", "arr", "cool" );
            $backend = new CSV_Backend( false );
            $keys = $backend->parse_keys( $arr );
            $this->assertTrue( $expected_result === $keys );
        }


        public function test_parse_values(){
            $arr = array(
                "home" => "casa",
                "group" => array(
                    "working" => "haciendo",
                    "again" => "otra vez",
                    "white" => array( "one", "two", "three" )
                ),
                "tired" => "cansado",
                "arr" => array( "uno", "dos", "tres" ),
                "cool" => "chevre"
            );
            $expected_result = array( "casa", "haciendo", "otra vez", "one,two,three", "cansado", "uno,dos,tres", "chevre" );
            $backend = new CSV_Backend( false );
            $values = $backend->parse_values( $arr );
            $this->assertTrue( $expected_result === $values );
        }


        public function test_create_csv(){
            $backend = new CSV_Backend( false );
            $result = $backend->create( CSV_OUTPUT_FILE );
            $this->assertTrue( true === is_custom_error( $result ) );
            
            $result = $backend->load( SPACEX_JSON );
            //var_dump( $result->getMessage() );
            $this->assertTrue( true === $result );
            
            $result = $backend->create( CSV_OUTPUT_FILE );
           // var_dump( $result->getMessage() );
            $this->assertTrue( true === $result );
            
        }


        public function test_yaml_evaluate(){
            $arr = array(
                "home" => "casa",
                "group" => array(
                    "working" => "haciendo",
                    "again" => "otra vez",
                    "white" => array( "one", "two", "three" )
                ),
                "tired" => "cansado",
                "arr" => array( "uno", "dos", "tres" ),
                "cool" => "chevre"
            );
            $expected_result = array(
                "home: casa",
                "group: ",
                "\tworking: haciendo",
                "\tagain: otra vez",
                "\twhite: ",
                "\t\t- one",
                "\t\t- two",
                "\t\t- three",
                "tired: cansado",
                "arr: ",
                "\t- uno",
                "\t- dos",
                "\t- tres",
                "cool: chevre",
            );
            $backend = new YAML_Backend( false );
            $result = $backend->evaluate( $arr );
            $this->assertTrue( $result === $expected_result );

            $arr = array(
                "home" => array(
                    array(
                        "working" => "haciendo",
                        "again" => "otra vez",
                    ),
                    array(
                        "november" => "noviembre",
                        "dangerous" => "peligroso",
                    ),
                ),
                "table" => "mes"
            );
            $expected_result = array(
                "home: ",
                "\t- working: haciendo",
                "\tagain: otra vez",
                "\t- november: noviembre",
                "\tdangerous: peligroso",
                "table: mes"
            );
            $result = $backend->evaluate( $arr );
            // var_dump( $result );
            // var_dump( $expected_result );
            $this->assertTrue( $result === $expected_result );
        }


        public function test_create_yaml(){
            $backend = new YAML_Backend( false );
            $result = $backend->create( YAML_OUTPUT_FILE );
            $this->assertTrue( true === is_custom_error( $result ) );
            
            $result = $backend->load( SPACEX_JSON );
            //var_dump( $result->getMessage() );
            $this->assertTrue( true === $result );
            
            $result = $backend->create( YAML_OUTPUT_FILE );
           // var_dump( $result->getMessage() );
            $this->assertTrue( true === $result );
            
        }
    }
?>