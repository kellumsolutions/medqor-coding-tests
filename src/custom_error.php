<?php

class CustomError extends Exception {
    public function __construct( $message, $code = 0, Throwable $previous = null ) {
        parent::__construct( $message, $code, $previous );
    }


    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}

function is_custom_error( $thing ){
    $is_custom_error = ( $thing instanceof CustomError );
    return $is_custom_error;
}

?>