<?php

    require_once( __DIR__ . "/csv_backend.php" );
    require_once( __DIR__ . "/yaml_backend.php" );
    require_once( __DIR__ . "/custom_error.php" );
    
    // Define constants.
    define( "SPACEX_JSON", __DIR__ . "/data/spacex.json" );
    define( "CSV_OUTPUT_FILE", __DIR__ . "/output/spacex.csv" );
    define( "YAML_OUTPUT_FILE", __DIR__ . "/output/spacex.yaml" );

    $csv = new CSV_Backend( SPACEX_JSON );
    $csv->create( CSV_OUTPUT_FILE );
    
    $yaml = new YAML_Backend( SPACEX_JSON );
    $yaml->create( YAML_OUTPUT_FILE );
?>