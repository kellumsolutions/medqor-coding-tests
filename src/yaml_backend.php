<?php

    require_once( __DIR__ . "/backend.php" );

    class YAML_Backend extends Backend {

        /**
         * Required function.
         * 
         * Where the magic happens. 
         * Should transform the data from the input file into the desired format.
         * 
         */
        public function format( $data ){
            $result = array();
            $indent_level = 0;

            return $this->evaluate( $data, $indent_level );
        }


        /**
         * Required function.
         * 
         * Saves data output by the format() method to a file.
         */
        public function save( $output, $data ){
            $fp = fopen( $output, 'w' );
            foreach ( $data as $index => $line ){
                fwrite( $fp, $line . "\n" );
            }
            fclose( $fp );
            return true;
        }


        /**
         * Formats data into yaml formatted arrays.
         * 
         * @param $data - What is being evaluated.
         * @param $indent_level - How many spaces to indent the data.
         * @param $prefix - The dash before items in an array. "" | "- " 
         * @param $show_key - Whether to show the key. Only not true for regular arrays.
         * @param $prefix_first - Whether to show the dash on the first written item. 
         * 
         */
        public function evaluate( $data, $indent_level = 0, $prefix = "", $show_key = true, $prefix_first = false ){
            $output = array();
            $indentation = $this->get_indentation( $indent_level );
            $iteration = 0; // Used to determine the $prefix_first.
            
            foreach( $data as $key => $value ){
                if ( is_array( $value ) ){
                    $is_associative_array = $this->is_assoc( $value );
                    $_indent = $indent_level;
                    $_prefix_first = false;

                    // If $value is an associative array and it has a prefix "-",
                    // (meaning its inside a regular array), put the prefix on the 
                    // first key of the associative array.
                    // Don't add to indentation for child data.
                    if ( $is_associative_array && !empty( $prefix ) ){
                        $_prefix_first = true;

                    } else {
                        // Write the key. 
                        $output[] = $indentation . $prefix . $key . ": ";
                        $_indent++;
                    }

                    $_prefix = $is_associative_array ? "" : "- ";
                    $_output = $this->evaluate( $value, $_indent, $_prefix, $is_associative_array, $_prefix_first );
                    
                    // Add each new line to the output.
                    foreach ( $_output as $_o ){
                        $output[] = $_o;
                    }

                // Regular scalar values.
                } else {
                    $_key = $show_key ? $key . ": " : "";
                    $_prefix = 1 >= count( $data ) ? "" : $prefix;
                    if ( empty( $_prefix ) && $prefix_first && $iteration === 0 ){
                        $_prefix = "- ";
                    }
                    $output[] = $indentation . $_prefix . $_key . $value;
                }
                $iteration++;
            }
            return $output;
        }


        public function get_indentation( $level, $char = "\t" ){
            $output = "";
            for ( $i = 0;$i<$level;$i++ ){
                $output .= $char;
            }
            return $output;
        }
    }
?>