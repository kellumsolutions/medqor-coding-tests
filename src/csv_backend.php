<?php

    require_once( __DIR__ . "/backend.php" );

    class CSV_Backend extends Backend {

        /**
         * Required function.
         * 
         * Where the magic happens. 
         * Should transform the data from the input file into the desired format.
         * 
         */
        public function format( $data ){
            $result = array();
            $headers = $this->parse_keys( $data[ "data" ][ "launches" ][0] );

            $body = array();
            foreach( $data[ "data" ][ "launches" ] as $index => $value ){
                $output = $this->parse_values( $value );

                if ( count( $output ) !== count( $headers ) ){
                    return new CustomError( "There was a mismatch between the header count and data count." );
                }
                $body[] = $output;
            }

            $result[] = $headers;
            foreach ( $body as $_body ){
                $result[] = $_body;
            }
            
            return $result;
        }


        /**
         * Required function.
         * 
         * Saves data output by the format() method to a file.
         */
        public function save( $output, $data ){
            $fp = fopen( $output, 'w' );
            foreach ( $data as $index => $line ){
                fputcsv( $fp, $line );
            }
            fclose( $fp );
            return true;
        }


        /**
         * Helper functions.
         */

        public function parse_keys( $data ){
            $output = array();

            foreach ( $data as $key => $value ){
                if ( is_array( $value ) && $this->is_assoc( $value ) ){
                    $_output = $this->parse_keys( $value );
                    foreach ( $_output as $_key ){
                        $output[] = $_key;
                    }
                } else {
                    $output[] = $key;
                }
            }
            return $output;
        }


        public function parse_values( $data ){
            $output = array();

            foreach ( $data as $key => $value ){
                if ( is_array( $value ) ){
                    if ( $this->is_assoc( $value ) ){
                        $_output = $this->parse_values( $value );
                        foreach ( $_output as $_value ){
                            $output[] = $_value;
                        }
                    } else {
                        $output[] = join( ",", $value );
                    }

                } else {
                    $output[] = $value;
                }
            }
            return $output;
        }
    }
?>