<?php

    class Backend {
        
        public $data_was_loaded = false;

        public $data = array();

        private $json = false;


        public function __construct( $json ){
            $this->json = $json;
        }


        public function load( $file = false ){
            $json = empty( $file ) ? $this->json : $file;

            if ( empty( $json ) ){
                return new CustomError( "No json file given in constructor." );
            }

            if ( false === file_exists( $json ) ){
                return new CustomError( "Could not load json file {$json}." );
            }

            $data = file_get_contents( $json );

            if ( false === $data ){
                $this->data_was_loaded = false;
                return new CustomError( "Invalid data loaded from json file." );
            }

            $this->data = json_decode( $data, true );

            if ( empty( $this->data ) ){
                $this->data_was_loaded = false;
                return new CustomError( "No data loaded from json file." );
            }

            $this->data_was_loaded = true;
            return true;
        }


        public function create( $output ){
            if ( false === $this->data_was_loaded ){

                // Try to load json file.
                if ( is_custom_error( $result = $this->load() ) ){
                    return $result;
                }
            }

            $result = $this->maybe_create_output_file( $output );
            if ( is_custom_error( $result ) ){
                return $result;
            }

            $data = $this->format( $this->data );
            
            if ( is_custom_error( $data ) ){
                return $data;
            }

            if ( empty( $data ) ){
                return new CustomError( "Transformed data is empty." );
            }

            $result = $this->save( $output, $data );
            if ( is_custom_error( $result ) ){
                return $result;
            }
            if ( empty( $result ) ){
                return new CustomError( "Save was unsuccessful." );
            }
            return true;
        }


        public function format( $data ){
            return new CustomError( "The format function must be overridden." );
        }


        public function save( $output, $data ){
            return new CustomError( "The save function must be overridden." );
        }


        private function maybe_create_output_file( $output ){
            $output_dir = dirname( $output );

            if ( false === is_dir( $output_dir ) ){
                if ( false === mkdir( $output_dir ) ){
                    // Error.
                    return new CustomError( "Could not create output dir {$output_dir}." );
                }
            } 
            if ( false === file_put_contents( $output, "" ) ){
                // Error.
                return new CustomError( "Could not create output file {$output}." );
            }
            return true;
        }


        public function is_assoc( $arr ){
            if ( array() === $arr ){ return false; }
            return array_keys( $arr ) !== range( 0, count( $arr ) - 1 );
        }
    }

?>